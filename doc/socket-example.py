#!/usr/bin/python3 -u
"""
Example Netify Socket Sink Reader
"""
import sys
import json
import socket

"""
Connect to Netify Agent via the Netify Sink Socket Plugin.

If only a path is supplied, created a UNIX socket.
If a host and port is supplied, create a TCP socket.
"""
def connect(hostpath: str, port: int = 0):
    try:
        if port == 0:
            sd = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sd.connect((hostpath))
        else:
            sd = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sd.connect((hostpath, port))

        return sd.makefile()

    except OSError as e:
        if port == 0:
            print(f"Error connecting to {hostpath}: {e}")
        else:
            print(f"Error connecting to {hostpath}:{port}: {e}")

    return None

"""
Verify command-line arguments.
"""
if len(sys.argv) < 2 or len(sys.argv) > 3:
    print(f"Usage: {sys.argv[0]} <host/path> [<port>]")
    sys.exit(1)

try:
    if len(sys.argv) == 2:
        sd = connect(sys.argv[1])
    else:
        sd = connect(sys.argv[1], int(sys.argv[2]))

    if sd is None:
        sys.exit(1)

    while True:
        """
        Step 1 - Read payload header.

        Every payload from the Agent is preceded by a header object.
        The header contains a single element: length
        The length indicates the following payload size in bytes.
        """
        line = sd.readline()
        if len(line) == 0:
            print("Connection closed.")
            sys.exit(0)

        header = json.loads(line.strip())
        if "length" not in header:
            print("Error: Malformed header.")
            sys.exit(1)

        """
        Step 2 - Read the payload.

        Using the length from the preceding header object, read the
        specified bytes of the payload object.
        """
        line = sd.read(header["length"])
        if len(line) == 0:
            print("Connection closed.")
            sys.exit(0)

        payload = json.loads(line.strip())

        if "type" not in payload:
            print("Error: Malformed payload.")
            sys.exit(1)

        """
        Step 3 - Process the payload.

        The message payload has been read, process the contents.
        In this example, we simply print the payload type.
        """
        print(f">>> {payload['type']}")

except KeyboardInterrupt:
    print()
    print("[interrupted]")
    sys.exit(0)

// Netify Agent Socket Sink Plugin
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fstream>

#include <nd-except.hpp>

#include "LUrlParser.hpp"
#include "nsp-plugin.hpp"

using namespace std;
using json = nlohmann::json;

#ifdef _ND_ENABLE_WEBSOCKETS
using namespace ix;

static void nspChannel_WebSocketClientMessage(
  shared_ptr<ConnectionState> connection_state,
  WebSocket &ws, const WebSocketMessagePtr &msg) {
    string tag(__PRETTY_FUNCTION__);
    auto ip = connection_state->getRemoteIp();

    if (msg->type == WebSocketMessageType::Open) {
        connection_state->computeId();
        auto connection_id = connection_state->getId();

        nd_dprintf("%s: socket connected: %s %s\n", tag.c_str(),
          ip.c_str(), connection_state->getId().c_str());
#if 0
        TLogger() << "remote ip: " << remoteIp;
        TLogger() << "id: " << connection_state->getId();
        TLogger() << "Uri: " << msg->openInfo.uri;
        TLogger() << "Headers:";
        for (auto it : msg->openInfo.headers)
        {
            TLogger() << it.first << ": " << it.second;
        }
#endif
    }
    else if (msg->type == WebSocketMessageType::Close) {
        nd_dprintf("%s: socket closed.\n", tag.c_str());
    }
    else if (msg->type == WebSocketMessageType::Message) {
        nd_dprintf("%s: client message.\n", tag.c_str());
    }
}
#endif

nspChannel::nspChannel(const string &channel,
  const json &jconf, const string &default_port)
  : channel(channel), bind_proto(BIND_UNKNOWN),
    bind_port(default_port), server(nullptr)
#ifdef _ND_ENABLE_WEBSOCKETS
    ,
    server_ws(nullptr), tls_verify_host(true)
#endif
{
    string addr;
    auto it = jconf.find("bind_address");
    if (it != jconf.end()) {
        nd_expand_variables(it->get<string>(), addr, ndGC.conf_vars);
    }

    if (addr.empty()) {
        throw ndException(
          "%s: required parameter missing: %s",
          channel.c_str(), "bind_addr");
    }

    const auto url = ParseURL::parseURL(addr);

    if (! url.isValid()) {
        throw ndException("%s: invalid parameter: %s",
          channel.c_str(), "bind_addr");
    }

    if (url.scheme_ == "unix") {
        bind_proto = BIND_UNIX;
        bind_addr = url.path_;
    }
    else if (url.scheme_ == "tcp") {
        bind_proto = BIND_TCP;
        bind_addr = url.host_;
        bind_port = url.port_;
    }
#ifdef _ND_ENABLE_WEBSOCKETS
    else if (url.scheme_ == "ws") {
        bind_proto = BIND_TCP_WS;
        bind_addr = url.host_;
        bind_port = url.port_;
    }
    else if (url.scheme_ == "wss") {
        bind_proto = BIND_TCP_WSS;
        bind_addr = url.host_;
        bind_port = url.port_;
    }
#endif
    else {
        throw ndException("%s: invalid %s value: %s",
          channel.c_str(), "bind_proto", url.scheme_.c_str());
    }

    if (bind_addr.empty()) {
        throw ndException("%s: no %s value set.",
          channel.c_str(), "bind_addr");
    }
#ifdef _ND_ENABLE_WEBSOCKETS
    //    if (bind_proto == BIND_TCP_WS || bind_proto ==
    //    BIND_TCP_WSS) {
    //    }

    if (bind_proto == BIND_TCP_WSS) {
        it = jconf.find("tls_cert");
        if (it != jconf.end() && it->type() == json::value_t::string)
        {
            tls_cert = it->get<string>();
            nd_expand_variables(it->get<string>(), tls_cert,
              ndGC.conf_vars);
        }

        it = jconf.find("tls_key");
        if (it != jconf.end() && it->type() == json::value_t::string)
        {
            nd_expand_variables(it->get<string>(), tls_key,
              ndGC.conf_vars);
        }

        it = jconf.find("tls_ca");
        if (it != jconf.end() && it->type() == json::value_t::string)
        {
            nd_expand_variables(it->get<string>(), tls_ca,
              ndGC.conf_vars);
        }
        else tls_ca = "SYSTEM";

        it = jconf.find("tls_verify_peer");
        if (it != jconf.end() && it->type() == json::value_t::boolean &&
          ! it->get<bool>())
        {
            tls_ca = "NONE";
            tls_verify_host = false;
        }
    }
#endif
}

void nspChannel::CreateSockets(void) {
    if (bind_proto == BIND_UNIX) {
        nd_dprintf("%s: creating socket: %s\n",
          channel.c_str(), bind_addr.c_str());
        nspSocketServerLocal *skt;
        skt = new nspSocketServerLocal(bind_addr);
        skt->SetBlockingMode(false);
        server = skt;
    }
    else if (bind_proto == BIND_TCP) {
        nd_dprintf("%s: creating socket: %s:%s\n",
          channel.c_str(), bind_addr.c_str(), bind_port.c_str());
        nspSocketServerRemote *skt;
        skt = new nspSocketServerRemote(bind_addr, bind_port);
        skt->SetBlockingMode(false);
        server = skt;
    }
    else if (bind_proto == BIND_TCP_WS || bind_proto == BIND_TCP_WSS)
    {
        nd_dprintf("%s: creating %swebsocket: %s:%s\n",
          channel.c_str(),
          (bind_proto == BIND_TCP_WSS) ? "secure " : "",
          bind_addr.c_str(), bind_port.c_str());
#ifndef _ND_ENABLE_WEBSOCKETS
        throw ndException("%s: websockets not supported",
          channel.c_str());
#else
        char buffer[1024];
        struct servent se, *se_result = nullptr;

        uint16_t port = (uint16_t)strtoul(bind_port.c_str(),
          nullptr, 0);

        if (port == 0 &&
          getservbyname_r(bind_port.c_str(), nullptr, &se,
            buffer, sizeof(buffer), &se_result) != 0)
            port = (uint16_t)ntohs(se_result->s_port);

        server_ws = new WebSocketServer(port, bind_addr);

        server_ws->setOnClientMessageCallback(
          nspChannel_WebSocketClientMessage);

        auto rc = server_ws->listen();
        if (! rc.first) {
            throw ndException("%s: listen: %s\n",
              channel.c_str(), rc.second.c_str());
        }

        server_ws->setUserAgent(nd_get_version_and_features());

        if (bind_proto == BIND_TCP_WSS) {
            SocketTLSOptions options;

            options.certFile = tls_cert;
            options.keyFile = tls_key;
            options.caFile = tls_ca;
            options.tls = true;
            options.disable_hostname_validation = ! tls_verify_host;

            server_ws->setTLSOptions(options);
        }

        server_ws->start();
#endif
    }
}

void nspChannel::CloseSockets(void) {
    for (auto &i : clients) delete i.second;
    clients.clear();

    if (server != nullptr) {
        delete reinterpret_cast<nspSocket *>(server);
        server = nullptr;
    }
#ifdef _ND_ENABLE_WEBSOCKETS
    if (server_ws != nullptr) {
        server_ws->stop();
        delete server_ws;
        server_ws = nullptr;
    }
#endif
    for (auto &i : buffers) delete i.second;
    buffers.clear();
}

void nspChannel::PollSockets(void) {
    fd_set fds_read, fds_write;
    nspSocketClientMap::iterator ci;
    nspSocketBufferMap::iterator bi;

    int rc_read = -1, rc_write = -1;
    int server_fd = server->GetDescriptor();
    int max_read_fd = server_fd, max_write_fd = -1;

    FD_ZERO(&fds_read);
    FD_ZERO(&fds_write);

    for (ci = clients.begin(); ci != clients.end(); ci++) {
        FD_SET(ci->first, &fds_read);
        FD_SET(ci->first, &fds_write);
        if (ci->first > max_read_fd)
            max_read_fd = ci->first;
        if (ci->first > max_write_fd)
            max_write_fd = ci->first;

        bi = buffers.find(ci->first);
        if (bi == buffers.end()) {
            throw ndException("%s: %s: %s", channel.c_str(),
              "buffers.find", strerror(ENOENT));
        }

        int fd = bi->second->GetDescriptor();
        FD_SET(fd, &fds_read);
        if (fd > max_read_fd) max_read_fd = fd;
    }

    FD_SET(server_fd, &fds_read);

    struct timeval tv = { 0, 0 };
    rc_read = select(max_read_fd + 1, &fds_read, nullptr,
      nullptr, &tv);

    if (rc_read == -1 && errno != EINTR) {
        throw ndExceptionSystemError(channel.c_str(),
          "read select");
    }

    rc_write = 0;

    if (clients.size()) {
        tv = { 0, 0 };
        rc_write = select(max_write_fd + 1, nullptr,
          &fds_write, nullptr, &tv);

        if (rc_write == -1 && errno != EINTR) {
            throw ndExceptionSystemError(channel.c_str(),
              "write select");
        }
    }

    ci = clients.begin();

    while (rc_write > 0 && ci != clients.end()) {
        bool hangup = false;

        if (FD_ISSET(ci->first, &fds_read)) {
            ClientHangup(ci);
            if (--rc_read == 0) break;
            continue;
        }

        bi = buffers.find(ci->first);
        if (bi == buffers.end()) {
            throw ndException("%s: %s: %s", channel.c_str(),
              "buffers.find", strerror(ENOENT));
        }

        if (FD_ISSET(bi->second->GetDescriptor(), &fds_read) &&
          FD_ISSET(ci->first, &fds_write))
        {
            rc_write--;

            ssize_t length = 0;
            const uint8_t *p = nullptr;

            bi->second->BufferQueueFlush();
            p = bi->second->GetBuffer(length);

            while (p != nullptr && length > 0) {
                try {
                    ssize_t bytes = ci->second->Write(p, length);

                    if (bytes > 0) bi->second->Pop(bytes);

                    if (bytes != length) break;
                }
                catch (nspSocketHangupException &e) {
                    hangup = true;
                    ClientHangup(ci);
                    break;
                }
                catch (ndException &e) {
                    nd_dprintf("%s: %s\n", e.what());

                    hangup = true;
                    ClientHangup(ci);
                    break;
                }

                bi->second->BufferQueueFlush();
                p = bi->second->GetBuffer(length);
            }

            if (--rc_read == 0) break;
        }

        if (! hangup) ci++;
    }

    if (FD_ISSET(server_fd, &fds_read)) {
        try {
            ClientAccept();
        }
        catch (runtime_error &e) {
            nd_printf(
              "%s: Error accepting socket connection: %s\n",
              channel.c_str(), e.what());
        }
    }
}

void nspChannel::QueuePayload(const ndPluginSinkPayload::Ptr &payload) {
    nspSocketBufferMap::iterator bi;

    for (bi = buffers.begin(); bi != buffers.end(); bi++) {
        uint8_t *data = new uint8_t[payload->length];
        memcpy(data, payload->data, payload->length);
        bi->second->Push(payload->length, data);
    }
}

void nspChannel::ClientAccept(void) {
    nspSocket *client = nullptr;

    try {
        client = server->Accept();
    }
    catch (exception &e) {
        if (client != nullptr) delete client;
        throw;
    }

    nspSocketBuffer *buffer = new nspSocketBuffer();
    buffers[client->GetDescriptor()] = buffer;
    clients[client->GetDescriptor()] = client;
}

void nspChannel::ClientHangup(nspSocketClientMap::iterator &ci) {
    nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    delete ci->second;
    auto bi = buffers.find(ci->first);
    clients.erase(ci++);

    if (bi == buffers.end()) {
        throw ndException("%s: %s: %s", channel.c_str(),
          "buffers.find", strerror(ENOENT));
    }
    else {
        delete bi->second;
        buffers.erase(bi);
    }
}

nspPlugin::nspPlugin(const string &tag, const ndPlugin::Params &params)
  : ndPluginSink(tag, params) {
    reload = true;

    nd_dprintf("%s: initialized\n", tag.c_str());
}

nspPlugin::~nspPlugin() {
    Join();

    for (auto &c : channels) c.second.CloseSockets();

    nd_dprintf("%s: destroyed\n", tag.c_str());
}

void *nspPlugin::Entry(void) {
    nd_printf(
      "%s: %s v%s Copyright (C) 2021-2024 eGloo "
      "Incorporated.\n",
      tag.c_str(), PACKAGE_NAME, PACKAGE_VERSION);

    while (! ShouldTerminate()) {
        if (reload.load()) {
            Reload();
            reload = false;
        }

        if (WaitOnPayloadQueue()) {
            ndPluginSinkPayload::Ptr p;
            while (PopPayloadQueue(p)) {
#if 0
                nd_dprintf("%s: payload, length: %lu, %p\n",
                    tag.c_str(), p->length, p->data
                );
#endif
                for (auto &c : p->channels) {
                    auto channel = channels.find(c);
                    if (channel == channels.end()) continue;

                    switch (channel->second.bind_proto) {
                    case nspChannel::BIND_UNIX:
                    case nspChannel::BIND_TCP:
                        channel->second.QueuePayload(p);
                        break;
                    case nspChannel::BIND_TCP_WS:
                    case nspChannel::BIND_TCP_WSS:
#ifdef _ND_ENABLE_WEBSOCKETS
                        for (auto &&client :
                          channel->second.server_ws->getClients())
                        {
                            string data;
                            data.assign((char *)p->data, p->length);
                            client->send(data, false);
                        }
#endif
                        break;
                    default: break;
                    }
                }
            }
        }

        for (auto &channel : channels) {
            switch (channel.second.bind_proto) {
            case nspChannel::BIND_UNIX:
            case nspChannel::BIND_TCP:
                channel.second.PollSockets();
                break;
            default: break;
            }
        }
    }

    return nullptr;
}

void nspPlugin::GetVersion(string &version) {
    version = PACKAGE_VERSION;
}

void nspPlugin::GetStatus(nlohmann::json &status) {
#if 0
    lock_guard<mutex> lg(status_mutex);

    status["license_status"] = nlm.GetLicenseStatus(license_status);
    status["license_status_id"] = license_status;

    status["aggregator"] = aggregator;
    status["samples"] = samples.size();
#endif
}

void nspPlugin::DisplayStatus(const nlohmann::json &status) {
#if 0
    unsigned a = 0, s = 0;

    auto i = status.find("license_status_id");
    if (i != status.end() && i->is_number_unsigned())
        nlm.DisplayLicenseStatus(i->get<unsigned>());

    i = status.find("aggregator");
    if (i != status.end() && i->is_number_unsigned())
        a = i->get<unsigned>();

    i = status.find("samples");
    if (i != status.end() && i->is_number_unsigned())
        s = i->get<unsigned>();

    fprintf(stdout, "%s aggregator #%u\n", ndTerm::Icon::INFO, a);
    fprintf(stdout, "%s samples: %u\n", ndTerm::Icon::INFO, s);
#endif
}

void nspPlugin::DispatchEvent(ndPlugin::Event event, void *param) {
    switch (event) {
    case ndPlugin::Event::RELOAD: reload = true; break;
    default: break;
    }
}

void nspPlugin::Reload(void) {
    nd_dprintf("%s: Loading configuration: %s\n",
      tag.c_str(), conf_filename.c_str());

    json j;

    ifstream ifs(conf_filename);
    if (! ifs.is_open()) {
        nd_printf(
          "%s: Error loading configuration: %s: %s\n",
          tag.c_str(), conf_filename.c_str(), strerror(ENOENT));
        return;
    }

    try {
        ifs >> j;
    }
    catch (exception &e) {
        nd_printf(
          "%s: Error loading configuration: %s: JSON parse "
          "error\n",
          tag.c_str(), conf_filename.c_str());
        nd_dprintf("%s: %s: %s\n", tag.c_str(),
          conf_filename.c_str(), e.what());
        return;
    }

    string default_port = to_string(_NSP_DEFAULT_TCP_PORT);

    auto i = j.find("default_port");
    if (i != j.end() && i->type() == json::value_t::number_unsigned)
        default_port = to_string(i->get<unsigned>());
    else if (i != j.end() && i->type() == json::value_t::string)
        default_port = i->get<string>();

    for (auto &c : channels) c.second.CloseSockets();

    channels.clear();

    for (auto &kvp : j["channels"].get<json::object_t>()) {
        if (kvp.second.type() != json::value_t::object)
            continue;
        auto it = kvp.second.find("enable");
        if (it != kvp.second.end()) {
            if (it->type() == json::value_t::boolean &&
              it->get<bool>() != true)
                continue;
        }
        channels.insert(make_pair(kvp.first,
          nspChannel(kvp.first, kvp.second, default_port)));
    }

    for (auto &c : channels) c.second.CreateSockets();
}

ndPluginInit(nspPlugin);

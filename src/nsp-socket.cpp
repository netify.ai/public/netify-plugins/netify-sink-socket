// Netify Socket Sink Plugin
// Copyright (C) 2015-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

#define __FAVOR_BSD
#include <netinet/in.h>
#include <netinet/ip.h>
#undef __FAVOR_BSD

#include <nd-except.hpp>
#include <nd-util.hpp>

#include "nsp-socket.hpp"

#ifndef UNIX_PATH_MAX
#define UNIX_PATH_MAX 104
#endif

using namespace std;

#define _NSP_SOCKET_PROC_NET_UNIX "/proc/net/unix"

nspSocketLocal::nspSocketLocal(nspSocket *base, const string &node)
  : base(base), valid(false) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
    struct sockaddr_un *sa_un = new struct sockaddr_un;

    base->node = node;
    base->sa_size = sizeof(struct sockaddr_un);
    base->sa = (struct sockaddr_storage *)sa_un;

    memset(sa_un, 0, base->sa_size);

    sa_un->sun_family = base->family = AF_LOCAL;
    strncpy(sa_un->sun_path, base->node.c_str(), UNIX_PATH_MAX);

    int rc;

    if ((rc = IsValid()) != 0) {
        throw ndException("%s: invalid node: %s\n",
          __PRETTY_FUNCTION__, node.c_str());
    }

    valid = true;

    base->Create();
}

nspSocketLocal::~nspSocketLocal() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
    if (valid && base->type == ndSOCKET_TYPE_SERVER)
        unlink(base->node.c_str());
}

#ifdef __FreeBSD__
int nspSocketLocal::IsValid(void) {
    // TODO: Need a "BSD-way" to achieve the same...
    int rc = unlink(base->node.c_str());
    if (rc != 0 && errno != ENOENT) {
        nd_printf(
          "Error while removing stale socket file: %s: "
          "%s\n",
          base->node.c_str(), strerror(errno));
        return rc;
    }
    return 0;
}
#else
int nspSocketLocal::IsValid(void) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    struct stat socket_stat;

    if (base->type == ndSOCKET_TYPE_CLIENT) {
        stat(base->node.c_str(), &socket_stat);
        return errno;
    }
    else if (base->type == ndSOCKET_TYPE_SERVER) {
        int rc = 0;
        long max_path_len = 4096;

        max_path_len = pathconf(base->node.c_str(), _PC_PATH_MAX);
        if (max_path_len == -1) return errno;

        FILE *fh = fopen(_NSP_SOCKET_PROC_NET_UNIX, "r");
        if (! fh) return errno;

        for (;;) {
            char filename[max_path_len];
            unsigned int a, b, c, d, e, f, g;
            int count = fscanf(fh, "%x: %u %u %u %u %u %u ",
              &a, &b, &c, &d, &e, &f, &g);
            if (count == 0) {
                if (! fgets(filename, max_path_len, fh))
                    break;
                continue;
            }
            else if (count == -1) break;
            else if (! fgets(filename, max_path_len, fh))
                break;
            else if (strncmp(filename, base->node.c_str(),
                       base->node.size()) == 0)
            {
                rc = EADDRINUSE;
                break;
            }
        }

        fclose(fh);

        if (rc != 0) return rc;

        if (stat(base->node.c_str(), &socket_stat) != 0 &&
          errno != ENOENT)
            return errno;

        unlink(base->node.c_str());
    }

    return 0;
}
#endif

nspSocketRemote::nspSocketRemote(nspSocket *base,
  const string &node, const string &service)
  : base(base) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    base->node = node;
    base->service = service;

    base->Create();
}

nspSocketRemote::~nspSocketRemote() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketClient::nspSocketClient(nspSocket *base)
  : base(base) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    base->type = ndSOCKET_TYPE_CLIENT;
}

nspSocketClient::~nspSocketClient() {
}

nspSocket *nspSocketServer::Accept(void) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
    nspSocket *peer = nullptr;
    int peer_sd = -1;
    socklen_t peer_sa_size = 0;
    sockaddr *peer_sa = nullptr;

    if (base->sa_size == sizeof(struct sockaddr_un)) {
        peer_sa = (sockaddr *)new struct sockaddr_un;
        peer_sa_size = sizeof(struct sockaddr_un);
    }
    else {
        peer_sa = (sockaddr *)new struct sockaddr_storage;
        peer_sa_size = sizeof(struct sockaddr_storage);
    }

    try {
        peer_sd = accept(base->sd, peer_sa, &peer_sa_size);
        if (peer_sd < 0) {
            throw ndExceptionSystemError(__PRETTY_FUNCTION__,
              "accept");
        }

        if (base->sa_size == sizeof(struct sockaddr_un)) {
            peer = new nspSocket(base->node);

            nd_dprintf("%s: peer: %s\n",
              __PRETTY_FUNCTION__, base->node.c_str());
        }
        else {
            char node[NI_MAXHOST], service[NI_MAXSERV];

            int rc = getnameinfo(peer_sa, peer_sa_size,
              node, NI_MAXHOST, service, NI_MAXSERV,
              NI_NUMERICHOST | NI_NUMERICSERV);

            if (rc != 0) {
                throw ndExceptionSystemErrno(
                  __PRETTY_FUNCTION__, "getnameinfo", rc);
            }

            peer = new nspSocket(node, service);

            nd_dprintf("%s: peer: %s:%s\n",
              __PRETTY_FUNCTION__, node, service);
        }

        peer->sd = peer_sd;
        peer->family = base->family;
        peer->type = ndSOCKET_TYPE_CLIENT;
        peer->state = ndSOCKET_STATE_ACCEPTED;

        delete peer_sa;
    }
    catch (runtime_error &e) {
        if (peer != nullptr) {
            delete peer;
            peer = nullptr;
        }
        else if (peer_sa != nullptr) delete peer_sa;
        if (peer_sd >= 0) close(peer_sd);
        throw;
    }

    return peer;
}

nspSocketServer::nspSocketServer(nspSocket *base)
  : base(base) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
    base->type = ndSOCKET_TYPE_SERVER;
}

nspSocketServer::~nspSocketServer() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketClientLocal::nspSocketClientLocal(const string &node)
  : nspSocketClient(this), nspSocketLocal(this, node) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketClientLocal::~nspSocketClientLocal() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketServerLocal::nspSocketServerLocal(const string &node)
  : nspSocketServer(this), nspSocketLocal(this, node) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketServerLocal::~nspSocketServerLocal() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketClientRemote::nspSocketClientRemote(
  const string &node, const string &service)
  : nspSocketClient(this), nspSocketRemote(this, node, service) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketClientRemote::~nspSocketClientRemote() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketServerRemote::nspSocketServerRemote(
  const string &node, const string &service)
  : nspSocketServer(this), nspSocketRemote(this, node, service) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocketServerRemote::~nspSocketServerRemote() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocket::nspSocket()
  : sd(1), family(AF_UNSPEC), sa(nullptr), sa_size(0),
    type(ndSOCKET_TYPE_NULL), state(ndSOCKET_STATE_INIT),
    bytes_in(0), bytes_out(0) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocket::nspSocket(const string &node)
  : sd(-1), family(AF_UNSPEC), sa(nullptr), sa_size(0),
    node(node), type(ndSOCKET_TYPE_NULL),
    state(ndSOCKET_STATE_INIT), bytes_in(0), bytes_out(0) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocket::nspSocket(const string &host, const string &service)
  : sd(-1), family(AF_UNSPEC), sa(nullptr), sa_size(0),
    node(host), service(service), type(ndSOCKET_TYPE_NULL),
    state(ndSOCKET_STATE_INIT), bytes_in(0), bytes_out(0) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
}

nspSocket::~nspSocket() {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);
    if (sd != -1) close(sd);
    if (sa != nullptr) delete sa;
}

ssize_t nspSocket::Read(uint8_t *buffer, ssize_t length) {
    uint8_t *p = buffer;
    ssize_t bytes_read = 0, bytes_remaining = length;

    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    do {
        ssize_t rc = read(sd, p, bytes_remaining);

        if (rc < 0) {
            if (errno != EAGAIN && errno != EWOULDBLOCK)
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "read");
            break;
        }

        if (rc == 0) throw nspSocketHangupException("read");

        bytes_read += rc;
        p += rc;
        bytes_remaining -= rc;
        bytes_in += rc;
    }
    while (bytes_remaining > 0);

    return bytes_read;
}

ssize_t nspSocket::Write(const uint8_t *buffer, ssize_t length) {
    const uint8_t *p = buffer;
    ssize_t bytes_wrote = 0, bytes_remaining = length;

    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    do {
        ssize_t rc = write(sd, p, bytes_remaining);

        if (rc < 0) {
            if (errno != EAGAIN && errno != EWOULDBLOCK) {
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "write");
            }
            break;
        }

        if (rc == 0)
            throw nspSocketHangupException("write");

        bytes_wrote += rc;
        p += rc;
        bytes_remaining -= rc;
        bytes_out += rc;
    }
    while (bytes_remaining > 0);

    return bytes_wrote;
}

void nspSocket::SetBlockingMode(bool enable) {
    int flags;

    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    if (enable == false) {
        flags = fcntl(sd, F_GETFL);
        if (fcntl(sd, F_SETFL, flags | O_NONBLOCK) < 0) {
            throw ndExceptionSystemError(__PRETTY_FUNCTION__,
              "fcntl(F_SETFL, O_NONBLOCK)");
        }
    }
    else {
        flags = fcntl(sd, F_GETFL);
        flags &= ~O_NONBLOCK;
        if (fcntl(sd, F_SETFL, flags) < 0) {
            throw ndExceptionSystemError(__PRETTY_FUNCTION__,
              "fcntl(F_SETFL, ~O_NONBLOCK)");
        }
    }
}

void nspSocket::Create(void) {
    // nd_dprintf("%s\n", __PRETTY_FUNCTION__);

    if (family == AF_UNSPEC) {
        struct addrinfo hints;
        struct addrinfo *result, *rp;

        memset(&hints, 0, sizeof(struct addrinfo));
#if defined(__FreeBSD__)
        hints.ai_family = AF_UNSPEC;
#else
        hints.ai_family = AF_INET6;
#endif
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_V4MAPPED;
        // hints.ai_flags = AI_V4MAPPED | AI_ALL;
        if (type == ndSOCKET_TYPE_SERVER)
            hints.ai_flags |= AI_PASSIVE;
        hints.ai_protocol = IPPROTO_TCP;
        hints.ai_canonname = nullptr;
        hints.ai_addr = nullptr;
        hints.ai_next = nullptr;

        int rc;
        const char *_node = (node.length()) ? node.c_str() : nullptr;
        if ((rc = getaddrinfo(_node, service.c_str(),
               &hints, &result)) != 0)
        {
            throw ndException("%s: %s: %s", __PRETTY_FUNCTION__,
              "getaddrinfo", gai_strerror(rc));
        }

        sd = -1;
        for (rp = result; rp != nullptr; rp = rp->ai_next) {
            sd = socket(rp->ai_family,
              rp->ai_socktype | SOCK_NONBLOCK,
              rp->ai_protocol);
            if (sd < 0) {
                nd_printf("%s: socket: %s",
                  __PRETTY_FUNCTION__, strerror(errno));
                continue;
            }

            if (type == ndSOCKET_TYPE_CLIENT) {
                if (connect(sd, rp->ai_addr, rp->ai_addrlen) == 0)
                {
                    nd_printf("%s: connected\n", __PRETTY_FUNCTION__);
                    break;
                }
                else {
                    if (rp->ai_family == AF_INET) {
                        nd_printf("%s: connect v4: %s\n",
                          __PRETTY_FUNCTION__, strerror(errno));
                    }
                    else if (rp->ai_family == AF_INET6) {
                        nd_printf("%s: connect v6: %s\n",
                          __PRETTY_FUNCTION__, strerror(errno));
                    }
                    else {
                        nd_printf("%s: connect: %s\n",
                          __PRETTY_FUNCTION__, strerror(errno));
                    }
                }
            }
            else if (type == ndSOCKET_TYPE_SERVER) {
                int on = 1;
                if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR,
                      (char *)&on, sizeof(on)) != 0)
                {
                    throw ndExceptionSystemError(__PRETTY_FUNCTION__,
                      "setsockopt(SO_REUSEADDR)");
                }

                if (::bind(sd, rp->ai_addr, rp->ai_addrlen) == 0)
                    break;
                else {
                    throw ndExceptionSystemError(
                      __PRETTY_FUNCTION__, "bind");
                }
            }

            close(sd);
            sd = -1;
        }

        if (rp == nullptr) {
            freeaddrinfo(result);
            throw ndException("%s: %s: %s", __PRETTY_FUNCTION__,
              "getaddrinfo", "no addresses found");
        }

        family = rp->ai_family;
        sa_size = rp->ai_addrlen;

        sa = new struct sockaddr_storage;
        memcpy(sa, rp->ai_addr, sa_size);

        freeaddrinfo(result);

        if (sd < 0) {
            throw ndException("%s: %s", __PRETTY_FUNCTION__,
              "unable to create socket");
        }

        if (type == ndSOCKET_TYPE_SERVER) {
            if (::listen(sd, SOMAXCONN) != 0) {
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "listen");
            }
        }
    }
    else if (family == AF_LOCAL) {
        if ((sd = ::socket(family, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0)
        {
            throw ndExceptionSystemError(__PRETTY_FUNCTION__,
              "socket");
        }

        if (type == ndSOCKET_TYPE_CLIENT) {
            if (::connect(sd, (struct sockaddr *)sa, sa_size) != 0)
            {
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "connect");
            }
            nd_printf("%s: connected\n", __PRETTY_FUNCTION__);
        }
        else if (type == ndSOCKET_TYPE_SERVER) {
            if (::bind(sd, (struct sockaddr *)sa, sa_size) != 0)
            {
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "bind");
            }

            if (::listen(sd, SOMAXCONN) != 0) {
                throw ndExceptionSystemError(
                  __PRETTY_FUNCTION__, "listen");
            }
        }
    }

    // nd_dprintf("%s: created\n", __PRETTY_FUNCTION__);
}

nspSocketBuffer::nspSocketBuffer()
  : buffer(nullptr), fd_fifo{ -1, -1 },
    buffer_queue_offset(0), buffer_queue_length(0) {
    buffer = new uint8_t[_NSP_SOCKET_BUFSIZE];

    if (socketpair(AF_LOCAL, SOCK_STREAM | SOCK_NONBLOCK, 0,
          fd_fifo) < 0)
    {
        throw ndExceptionSystemError(__PRETTY_FUNCTION__,
          "socketpair");
    }
}

nspSocketBuffer::~nspSocketBuffer() {
    for (auto &i : buffer_queue) delete i.second;
    buffer_queue_offset = buffer_queue_length = 0;

    if (buffer != nullptr) delete[] buffer;
    if (fd_fifo[0] != -1) close(fd_fifo[0]);
    if (fd_fifo[1] != -1) close(fd_fifo[1]);
}

size_t nspSocketBuffer::BufferQueueFlush(void) {
    ssize_t bytes_wrote = 0;
    size_t bytes = 0, bytes_flushed = 0;

    while (buffer_queue.size()) {
        auto i = buffer_queue.front();
        bytes = i.first - buffer_queue_offset;

        bytes_wrote = send(fd_fifo[1],
          i.second + buffer_queue_offset, bytes, 0);

        if (bytes_wrote < 0) {
            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                // nd_dprintf("Unable to flush buffer queue to
                // client socket: %s\n",
                //     strerror(errno));
                break;
            }
            throw ndExceptionSystemError(__PRETTY_FUNCTION__,
              "send");
        }

        bytes_flushed += bytes_wrote;

        if ((size_t)bytes_wrote != bytes)
            buffer_queue_offset += bytes_wrote;
        else {
            buffer_queue_offset = 0;
            delete[] i.second;
            buffer_queue.pop_front();
        }
    }

    buffer_queue_length -= bytes_flushed;

    return bytes_flushed;
}

const uint8_t *nspSocketBuffer::GetBuffer(ssize_t &bytes) {
    bytes = recv(fd_fifo[0], buffer, _NSP_SOCKET_BUFSIZE, MSG_PEEK);
    if (bytes < 0) {
        if (errno == EAGAIN || errno == EWOULDBLOCK) {
            bytes = 0;
            return nullptr;
        }
        throw ndExceptionSystemError(__PRETTY_FUNCTION__,
          "recv");
    }
    else if (bytes == 0)
        throw nspSocketHangupException("recv");

    return (const uint8_t *)buffer;
}

void nspSocketBuffer::Push(size_t length, uint8_t *data) {
    buffer_queue.push_back(make_pair(length, data));
    buffer_queue_length += length;

    BufferQueueFlush();
}

void nspSocketBuffer::Pop(size_t bytes) {
    if (bytes == 0 || bytes > _NSP_SOCKET_BUFSIZE)
        throw ndExceptionSystemErrno(__PRETTY_FUNCTION__,
          "invalid size", EINVAL);

    ssize_t bytes_recv = recv(fd_fifo[0], buffer, bytes, 0);

    if (bytes_recv < 0) {
        throw ndExceptionSystemError(__PRETTY_FUNCTION__,
          "recv");
    }
    else if (bytes_recv == 0)
        throw nspSocketHangupException("recv");
    else if ((size_t)bytes_recv != bytes) {
        throw ndException("%s: short %s",
          __PRETTY_FUNCTION__, "recv");
    }
}

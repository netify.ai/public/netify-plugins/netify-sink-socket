#!/bin/sh -x

rm -rf src/IXWebSocket
mkdir -vp src/IXWebSocket

# Regenerate configuration files
find $(pwd) -name configure.ac | xargs touch

autoreconf -i --force -I m4 || exit 1


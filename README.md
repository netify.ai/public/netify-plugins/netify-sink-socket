# Netify Agent Log Sink Plugin
Copyright &copy; 2024 eGloo Incorporated

[![pipeline status](https://gitlab.com/netify.ai/public/netify-plugins/netify-sink-socket/badges/master/pipeline.svg)](https://gitlab.com/netify.ai/public/netify-plugins/netify-sink-socket/-/commits/master)

## Overview

This sink plugin accepts payloads from processor plugins and writes them to one or more configurable sockets.

## Documentation

Documentation can be found [here](doc/Netify%20Log%20Sink.md).


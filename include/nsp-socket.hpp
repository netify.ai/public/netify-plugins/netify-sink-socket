// Netify Agent Socket Sink Plugin
// Copyright (C) 2015-2024 eGloo Incorporated
// <http://www.egloo.ca>
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General
// Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public
// License along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#pragma once

#include <sys/socket.h>

#include <deque>
#include <unordered_map>

#include <nd-except.hpp>

class nspSocketHangupException : public std::runtime_error
{
public:
    explicit nspSocketHangupException(const std::string &what_arg)
      : runtime_error(what_arg) { }
};

class nspSocketLocal;
class nspSocketRemote;
class nspSocketClient;
class nspSocketServer;

enum nspSocketType {
    ndSOCKET_TYPE_NULL,
    ndSOCKET_TYPE_CLIENT,
    ndSOCKET_TYPE_SERVER
};

enum nspSocketState {
    ndSOCKET_STATE_INIT,
    ndSOCKET_STATE_CONNECTED,
    ndSOCKET_STATE_ACCEPTED,
    ndSOCKET_STATE_CLOSED,
};

class nspSocket
{
public:
    nspSocket();
    nspSocket(const std::string &node);
    nspSocket(const std::string &node, const std::string &service);
    virtual ~nspSocket();

    void SetBlockingMode(bool enable = false);

    ssize_t Read(uint8_t *buffer, ssize_t length);
    ssize_t Write(const uint8_t *buffer, ssize_t length);

    inline int GetDescriptor(void) { return sd; }

protected:
    friend class nspSocketLocal;
    friend class nspSocketRemote;
    friend class nspSocketClient;
    friend class nspSocketServer;

    void Create(void);

    int sd;
    int family;
    struct sockaddr_storage *sa;
    socklen_t sa_size;
    std::string node;
    std::string service;

    nspSocketType type;
    nspSocketState state;

    uint64_t bytes_in;
    uint64_t bytes_out;
};

class nspSocketLocal
{
protected:
    nspSocketLocal(nspSocket *base, const std::string &node);
    virtual ~nspSocketLocal();

    int IsValid(void);

    nspSocket *base;
    bool valid;
};

class nspSocketRemote
{
protected:
    nspSocketRemote(nspSocket *base,
      const std::string &node, const std::string &service);
    virtual ~nspSocketRemote();

    nspSocket *base;
};

class nspSocketClient
{
protected:
    nspSocketClient(nspSocket *base);
    virtual ~nspSocketClient();

    nspSocket *base;
};

class nspSocketServer
{
public:
    nspSocket *Accept(void);

    inline int GetDescriptor(void) { return base->sd; }

protected:
    nspSocketServer(nspSocket *base);
    virtual ~nspSocketServer();

    nspSocket *base;
};

class nspSocketClientLocal :
  public nspSocket,
  public nspSocketClient,
  protected nspSocketLocal
{
public:
    nspSocketClientLocal(const std::string &node);
    virtual ~nspSocketClientLocal();

protected:
};

class nspSocketServerLocal :
  public nspSocket,
  public nspSocketServer,
  protected nspSocketLocal
{
public:
    nspSocketServerLocal(const std::string &node);
    virtual ~nspSocketServerLocal();

protected:
};

class nspSocketClientRemote :
  public nspSocket,
  public nspSocketClient,
  protected nspSocketRemote
{
public:
    nspSocketClientRemote(const std::string &node,
      const std::string &service);
    virtual ~nspSocketClientRemote();

protected:
};

class nspSocketServerRemote :
  public nspSocket,
  public nspSocketServer,
  protected nspSocketRemote
{
public:
    nspSocketServerRemote(const std::string &node,
      const std::string &service);
    virtual ~nspSocketServerRemote();

protected:
};

#define _NSP_SOCKET_BUFSIZE 8192

class nspSocketBuffer
{
public:
    nspSocketBuffer();
    virtual ~nspSocketBuffer();

    inline int GetDescriptor(void) { return fd_fifo[0]; }
    const uint8_t *GetBuffer(ssize_t &bytes);

    size_t BufferQueueFlush(void);

    void Push(size_t length, uint8_t *data);
    void Pop(size_t bytes);

protected:
    uint8_t *buffer;
    int fd_fifo[2];

    size_t buffer_queue_offset;
    size_t buffer_queue_length;
    std::deque<std::pair<size_t, uint8_t *>> buffer_queue;
};

typedef std::unordered_map<int, nspSocket *> nspSocketClientMap;
typedef std::unordered_map<int, nspSocketBuffer *> nspSocketBufferMap;

// Netify Agent Socket Sink Plugin
// Copyright (C) 2021-2024 eGloo Incorporated
// <http://www.egloo.ca>

#pragma once

#include <nd-plugin.hpp>

#ifdef _ND_ENABLE_WEBSOCKETS
#include <ixwebsocket/IXWebSocketServer.h>
#endif

#include "nsp-socket.hpp"

constexpr uint16_t _NSP_DEFAULT_TCP_PORT = 1750;

class nspPlugin;

class nspChannel
{
public:
    nspChannel(const std::string &channel,
      const nlohmann::json &jconf, const std::string &default_port);
    virtual ~nspChannel() { CloseSockets(); }

    enum BindProtocol {
        BIND_UNKNOWN,
        BIND_UNIX,
        BIND_TCP,
        BIND_TCP_WS,
        BIND_TCP_WSS,
    };

    std::string channel;
    BindProtocol bind_proto;
    std::string bind_addr;
    std::string bind_port;

    void CreateSockets(void);
    void CloseSockets(void);

    void PollSockets(void);

    void QueuePayload(const ndPluginSinkPayload::Ptr &payload);

protected:
    friend class nspPlugin;

    nspSocketServer *server;
#ifdef _ND_ENABLE_WEBSOCKETS
    std::string connection_id;
    ix::WebSocketServer *server_ws;
    std::string tls_cert;
    std::string tls_key;
    std::string tls_ca;
    bool tls_verify_host;
#endif
    nspSocketClientMap clients;
    nspSocketBufferMap buffers;

    size_t QueueHeader(ndPluginSinkPayload *payload,
      nspSocketBufferMap::iterator &bi);

    void ClientAccept(void);
    void ClientHangup(nspSocketClientMap::iterator &ci);
};

class nspPlugin : public ndPluginSink
{
public:
    nspPlugin(const std::string &tag, const ndPlugin::Params &params);
    virtual ~nspPlugin();

    virtual void *Entry(void);

    virtual void DispatchEvent(ndPlugin::Event event,
      void *param = nullptr);

    virtual void GetVersion(std::string &version);

    virtual void GetStatus(nlohmann::json &status);
    virtual void DisplayStatus(const nlohmann::json &status);

protected:
    std::atomic<bool> reload;

    typedef std::map<std::string, nspChannel> Channels;
    Channels channels;

    void Reload(void);

    nlohmann::json status;
    std::mutex status_mutex;
};
